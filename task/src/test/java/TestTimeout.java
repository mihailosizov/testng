import org.testng.annotations.*;

public class TestTimeout {
    @Test(timeOut = 1000)
    public void waitLongTime() throws Exception {
        Thread.sleep(1001);
    }

    @Test(timeOut = 1000)
    public void waitShortTime() throws Exception {
        Thread.sleep(999);
    }

}
