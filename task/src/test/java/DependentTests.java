import org.testng.annotations.Test;

public class DependentTests {

    @Test(dependsOnMethods = {"beforeMethod1"})
    public void testMethod1() {
        System.out.println("This is testMethod1");
    }

    @Test(dependsOnGroups = {"init"})
    public void testMethod2() {
        System.out.println("This is testMethod2");
    }

    @Test
    public void beforeMethod1() {
        System.out.println("This test will be executed before dependent testMethod1");
    }

    @Test(groups = {"init"})
    public void beforeMethod2_belongsToInitGroup1() {
        System.out.println("This test will be executed before dependent testMethod2");
    }

    @Test(groups = {"init"})
    public void beforeMethod2_belongsToInitGroup2() {
        System.out.println("This test will be executed before dependent testMethod2");
    }


}