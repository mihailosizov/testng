import org.testng.annotations.*;

public class IgnoredTest {
    @Test(enabled = false)
    public void setProperty() {
        System.out.println("This method will be ignored!");
    }
}
