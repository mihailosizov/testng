import org.testng.annotations.*;
import org.testng.Assert;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SelectCasesTest extends Assert {

    @BeforeClass
    public void BeforeClass() throws IOException {
        PrepareInputFiles();
    }

    @AfterClass
    public void AfterClass() throws IOException {
        Clean();
    }

    @Test(priority = 0)
    public void CorrectInput() throws IOException {
        int rowAmount = 5;
        SelectCases.SelectCasesProgram("tmpfile1.ext", rowAmount);
        assertTrue(Files.exists(Paths.get("tmpfile1_res.ext")));    // check if result file exists
        assertEquals(RowCount(Paths.get("tmpfile1_res.ext").toFile()), (rowAmount + 1));    // check if amount of rows in result file equals to given row amount plus one title row
        assertTrue(RowsAreNotRepeating(Paths.get("tmpfile1_res.ext").toFile(), Paths.get("tmpfile1.ext").toFile())); // check if rows in result file are not containing rows from input file
    }

    @Test(priority = 1)
    public void CorrectDefaultRowAmountInput() throws IOException {
        SelectCases.SelectCasesProgram("tmpfile2.ext");
        assertTrue(Files.exists(Paths.get("tmpfile2_res.ext")));    // check if result file exists
        assertEquals(RowCount(Paths.get("tmpfile2_res.ext").toFile()), 11);     // check if amount of rows in result file equals to default value of 10 plus one title row
    }

    @Test(priority = 2)
    public void CorrectInputWithNoExtension() throws IOException {
        SelectCases.SelectCasesProgram("tmpfile3");
        assertTrue(Files.exists(Paths.get("tmpfile3_res")));    // check if result file exists
    }

    @Test(priority = 2, expectedExceptions = IndexOutOfBoundsException.class)
    public void BlankFileInput() throws IOException {
        SelectCases.SelectCasesProgram("tmpfile.blank.ext");
    }

    @Test(priority = 2, expectedExceptions = FileNotFoundException.class)
    void AbsentFileInput() throws IOException {
        SelectCases.SelectCasesProgram("tmpfile.absent.ext");
    }

    @Test(priority = 2, expectedExceptions = IllegalArgumentException.class)
    public void RowNumberIsMoreThanFileContains() throws IOException {
        SelectCases.SelectCasesProgram("tmpfile.1row.ext", 2);
    }

    public int RowCount(File file) throws IOException {
        LineNumberReader lnr = new LineNumberReader(new FileReader(file));
        lnr.skip(Long.MAX_VALUE); // move line reader to the very end of file
        int numberOfLines = lnr.getLineNumber();
        lnr.close();
        return numberOfLines;
    }

    public boolean RowsAreNotRepeating(File input, File output) throws IOException {
        List<String> inputLines = new ArrayList<>();
        List<String> outputLines = new ArrayList<>();
        String line;

        // read the file to the input
        BufferedReader bufferedReader = new BufferedReader(new FileReader(input));
        for (int i = 0; (line = bufferedReader.readLine()) != null; i++) {
            if (i > 0) {
                inputLines.add(line);
            }
        }
        bufferedReader.close();

        // read the file to the input
        bufferedReader = new BufferedReader(new FileReader(output));
        for (int i = 0; (line = bufferedReader.readLine()) != null; i++) {
            if (i > 0) {
                outputLines.add(line);
            }
        }
        bufferedReader.close();

        return Collections.disjoint(inputLines, outputLines);
    }

    public static void PrepareInputFiles() throws IOException {
        Random random = new Random();
        List<String> testInputLines = new ArrayList<>();
        Files.write(Paths.get("tmpfile.blank.ext"), testInputLines, StandardOpenOption.CREATE);
        testInputLines.add("case One Book Low Book Rating Product Date pairings");
        Files.write(Paths.get("tmpfile.1row.ext"), testInputLines, StandardOpenOption.CREATE);
        while (testInputLines.size() < 15) {
            int randomNumber = random.nextInt(100);
            if (!testInputLines.contains(randomNumber + " All All GTD Rating L|C|C | Index LM 3")) { // add only unique line to the list
                testInputLines.add(randomNumber + " All All GTD Rating L|C|C | Index LM 3");
            }
        }
        Files.write(Paths.get("tmpfile1.ext"), testInputLines, StandardOpenOption.CREATE);
        Files.write(Paths.get("tmpfile2.ext"), testInputLines, StandardOpenOption.CREATE);
        Files.write(Paths.get("tmpfile3"), testInputLines, StandardOpenOption.CREATE);
    }

    public static void Clean() throws IOException {
        Files.deleteIfExists(Paths.get("tmpfile1.ext"));
        Files.deleteIfExists(Paths.get("tmpfile2.ext"));
        Files.deleteIfExists(Paths.get("tmpfile3"));
        Files.deleteIfExists(Paths.get("tmpfile1_res.ext"));
        Files.deleteIfExists(Paths.get("tmpfile2_res.ext"));
        Files.deleteIfExists(Paths.get("tmpfile3_res"));
        Files.deleteIfExists(Paths.get("tmpfile.blank.ext"));
        Files.deleteIfExists(Paths.get("tmpfile.1row.ext"));
    }
}
